#!/usr/bin/env python

import os
import sys
import ast
import requests
# Do not forget to install yaml. pip install PyYAML
from pymongo import MongoClient

ACCESS_TOKENS = ['a35f354eba5a3015a09ec8deb8b6c79e',
    '2eb656be44652eb4ee56ae0e7db22c9f',
    '50dea2bcd31253cece5e3b83c77c110a',
    'e423966a33db1716f0f6929237b3b543',
    '97ae6fde5cc91747798f5411902567f7'
    ] 

url = 'https://api.angel.co/1/'
startup_id = 0
client = MongoClient()
db = client.database
posts = db.posts
token_id = 0

# Uncomment below code to unlease the daemon.
"""
def createDaemon(choice):
    if choice == 'start':
        try:
            pid = os.fork()
            if pid > 0:
               print 'PID: %s Daemon started successfully' % pid
               with open('pidinfo', 'w') as piddata:
                   piddata.write("%d" %pid)
               os._exit(0)
        except OSError, error:
            print 'Unable to fork. Error: %d (%s)' % (error.errno, error.strerror)
            os._exit(1)
        main()
    elif choice == 'stop':
        with open('pidinfo', 'r') as pid:
            os.kill(int(pid.read()), 15)
        print "Daemon killed succesfully"
    else:
        print "Wrong parameter."
        return
"""

# Uncomment below code to log all printed data to file.
"""
def log_it(logdata=None):
    with open("angelhack.log", "a") as fp:
        fp.write(logdata+'\n')
    return
"""

def crawlroles(startup_id=None, token_id=None, page=1, role_data=[]):
    """
    Get all info about past_investor, current investor, employees, founders
    """
    response = requests.get(url+"startup_roles?startup_id="+str(startup_id)+"&page="+str(page)+"?access_token="+ACCESS_TOKENS[token_id])
    data = ast.literal_eval(response.text.replace('false', '0').replace('null', '0').replace('true', '1'))
    role_data.extend(data.get("startup_roles"))
    if data.get("last_page") == data.get("page"):
        return role_data
    else:
        return crawlroles(startup_id, token_id, data.get("page")+1, role_data)

def getpresslinks(startup_id=None, token_id=None, page=1, press_links=[]):
    """
    Get all press links if available for a particular startups
    """
    response = requests.get(url+"press?startup_id="+str(startup_id)+"&page="+str(page)+"?access_token="+ACCESS_TOKENS[token_id])
    data = ast.literal_eval(response.text.replace('false', '0').replace('null', '0').replace('true', '1'))
    press_links.extend(data.get("press"))
    if data.get("last_page") == data.get("page"):
        return press_links
    else:
        return getpresslinks(startup_id, token_id, data.get("page")+1, press_links)

def crawl(startup_id=None, token_id=None):
    """
    Get all info about the startups
    """
    try:
        response = requests.get(url+"startups/"+str(startup_id)+"?access_token="+ACCESS_TOKENS[token_id])
        if response.status_code == 200:
            data = ast.literal_eval(response.text.replace('false', '0').replace('null', '0').replace('true', '1'))
            if data.get('hidden', 1) == 0:
                role_data = crawlroles(startup_id, token_id)
                press_links = getpresslinks(startup_id, token_id)
                data["roles"] = role_data
                data["press_links"] = press_links
                posts.insert(data)
                print("Inserted data for id: {0} Name: {1}".format(startup_id, data.get('name', "No name found")))
                # Uncomment below line to use daemonised code
                #log_it("Inserted data for id: {0} Name: {1}".format(startup_id, data.get('name', "No name found")))
    except requests.exceptions.ConnectionError:
        return 1
    else:
        return int(response.headers['x-ratelimit-remaining'])

def main():
    try:
        global startup_id, ACCESS_TOKENS, token_id
        while True:
            rate_limit_flag = crawl(startup_id, token_id)
            startup_id += 1
            if rate_limit_flag == 1:
                ACCESS_TOKENS.append(ACCESS_TOKENS[token_id])
                token_id += 1
                # comment below line to use daemonised code
                print("Switching to new access token: token_id: {0} token: {1}".format(token_id, ACCESS_TOKENS[token_id]))
                # Uncomment below line to use daemonised code
                #log_it("Switching to new access token: token_id: {0} token: {1}".format(token_id, ACCESS_TOKENS[token_id]))
    except KeyboardInterrupt:
        # comment below line to use daemonised code
        sys.exit(-1)
        # Uncomment below line use daemonised code
        #createDaemon("stop")

if __name__ == '__main__':
    # comment below line to use daemonised code
    main()
    # Uncomment below line to use daemonised code
    """
    if len(sys.argv) == 2:
        createDaemon(sys.argv[1])
    else:
        print "Wrong parameter."
    """
